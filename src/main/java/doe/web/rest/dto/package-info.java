/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package doe.web.rest.dto;
