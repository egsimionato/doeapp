(function() {
    'use strict';

    angular
        .module('doeappApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
